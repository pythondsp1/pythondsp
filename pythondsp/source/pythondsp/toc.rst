Table of Contents 
*****************

In this website, following programming languages are used for different purposes, 

* **C/C++** : 'To simulate the mathematical models of the design' and 'implementing the designs on FPGA using NIOS-II processor.'
* **Verilog/SystemVerilog/VHDL** : To implement the designs on FPGA board. 
* **Python** : 'To simulate the mathematical models of the designs', 'data processing' and for analyzing the data received by the FPGA'. 
* **HTML and JavaScript etc.** : 'To design the webpages.'


.. admonition:: Offline tutorials in PDF, EPUB and HTML formats
   :class: danger

   Click on the '**v.latest**' on the bottom-left in the tutorial website, and select the format for offline reading, as shown in below figure, 

   .. figure:: fig/download.png
   
      Download tutorials for offline reading 


Teachings and Quotes
====================

* `Goodread <http://goodread.readthedocs.io/en/latest/>`_
* `Meher Baba Prayers <http://meher-baba-prayers.readthedocs.io/en/latest/>`_ (`Download Kindle-format <https://drive.google.com/open?id=0B9zymgX8PsIXczFPRzJCOHdRTWc>`_)
  

Linux software list
===================

A list of software is added in the below link, which are used to create the tutorials presented in this website. The software lists are provided for 'Ubuntu', 'Lubuntu', 'Mint' and 'Fedora'.

.. note::

    I tried various other distros as well e.g. CentOS, Manjaro and OpenSUSE etc. But I like "Lubuntu" as it is lightweight and the software (related to Electronics designs) are very easy to install as compare to other distros. 

* `Linux software list <http://linux-software-list.readthedocs.io/en/latest/>`_
    
FPGA Designs with VHDL
======================

In this tutorial, VHDL is used to implement various designs on FPGA board. All the designs are tested on the FPGA boards. NIOS II processor is also used for designs. 

* Online : http://vhdlguide.readthedocs.io
* PDF   : https://drive.google.com/file/d/0B9zymgX8PsIXSjdLelNtOW5yYkU/
* CODES : https://drive.google.com/file/d/0B9zymgX8PsIXUFJCYjJIaTk2eUE/ 
* Latex : https://github.com/pythondsp/FPGA-designs-with-VHDL


.. _`fpga-designs-with-verilog-and-systemverilog`:

FPGA designs with Verilog and SystemVerilog
===========================================

In this tutorial, Verilog is used to implement various designs on FPGA board. All the designs are tested on the FPGA boards. NIOS II processor is also used for designs. 

* Online : http://verilogguide.readthedocs.io
* Online : http://verilogguide.readthedocs.io
* PDF    : https://drive.google.com/file/d/0B9zymgX8PsIXOWZWRXFUeF9mbVk/
* CODES  : https://drive.google.com/file/d/0B9zymgX8PsIXYXRUS0pmWkpGd0U/
* Latex  : https://github.com/PythonDSP/FPGA-designs-with-Verilog-and-SystemVerilog

FPGA designs with MyHDL
=======================

In this tutorial, the designs of `Verilog/SystemVerilog <http://pythondsp.readthedocs.io/en/latest/pythondsp/toc.html#fpga-designs-with-verilog-and-systemverilog>`_/`VHDL <http://pythondsp.readthedocs.io/en/latest/pythondsp/toc.html#fpga-designs-with-vhdl>`_-tutorials are re-implemented using MyHDL. 

* Online  : http://myhdlguide.readthedocs.io


Programming with C and C++
==========================

In this tutorial, basic features of C and C++ programming are discussed for designing the embedded systems using NIOS.

* Online : http://cppguide.readthedocs.io
* PDF   : https://drive.google.com/open?id=0B9zymgX8PsIXZVJEREl2WVlGbXM
* CODES : https://drive.google.com/open?id=0B9zymgX8PsIXd256YWFhM1dkU3M
* Latex : https://github.com/PythonDSP/Programming-with-C-and-CPP


Python
======

In the below topics, several features of Python are discussed which can be useful for 'data processing' and 'simulation of mathematical designs". 

Advance Python
--------------

In this tutorial, basic Python features are reviewed first. And then the advance Python features are discussed such as decorator, descriptor and property etc.  

* `Advance Python Tutorials <http://pythonguide.readthedocs.io/en/latest/>`_


Python for simulation
---------------------

In this tutorial, Python is discussed for simulating the mathematical designs. Further, Numba and Cython are used to speed up the simulation; and Matplotlib is used for plotting the data. Lastly, OOP method is also discussed and used for simulations. 

* PDF   : https://drive.google.com/file/d/0B9zymgX8PsIXNUlHM2Z1T1A2NEE/ 
* CODES : https://drive.google.com/file/d/0B9zymgX8PsIXRV9zVGVaTWdVZnc/ 
* Latex : https://github.com/pythondsp/Python3-Simulation-with-OOP


Matplotlib
----------

In this tutorial, the Matplotlib library is discussed to plot the data in Python, 

* `Matplotlib Guide <http://matplotlibguide.readthedocs.io/en/latest/>`_


Pandas-0.22.0
-------------

In this tutorial, Pandas library is discussed for data processing. Created using Python-3.6.4 and Pandas-0.22.0

* `Pandas guide <http://pandasguide.readthedocs.io/en/latest/index.html>`_

Machine learning
----------------

In this tutorial, the SciKit library is discussed for Machine learning.

* `Machine learning guide <http://mclguide.readthedocs.io/en/latest/>`_


Regular expressions
-------------------

Some useful regular expression patterrns are shown here, 

* `Regular expression guide <http://regexguide.readthedocs.io/en/latest/index.html>`_
  
Statistical analysis
--------------------

Here, basics of random variable and their probability distribution functions are discussed. Then PDF and CDF of these distributions are implemented using Python. 

* `Stochastic processes and data mining with python <http://DataMiningGuide.readthedocs.io/en/latest/index.html>`_
  

Testing with PyTest
-------------------

In the below tutorial, 'PyTest' library is used to test the Python and Cython codes. 

Also, we can see the methods by which Cython codes can be documented on the ReadTheDocs (see conf.py on the Git-repository). Please see the `'Documentation Guide' <http://docguide.readthedocs.io/en/latest/index.html>`_ for more details about the auto-documentation of Python and Cython codes. 

* `PyTest Guide : py.test and numpy.testing <http://pytestguide.readthedocs.io/en/latest/index.html>`_
  

GUI (under progress)
--------------------

* `GUI with PyQT and Tkinter <http://guiguide.readthedocs.io/en/latest/index.html>`_

MySQL with Python
=================

Here, various MySQL commands are discussed. Further, Python is used to connect with the MySQL database. 

* `MySQL with python guide <http://mysqlguide.readthedocs.io/en/latest/index.html>`_


Guides
======
 
 Following are the short guides, which contain some useful tips/information for using Git, Unix and ReadTheDocs. Also, good ways of creating the simulators are discussed. 

Simulation guide
----------------

* `Simulation Guide : Guideline for designing simulators <http://simulationguide.readthedocs.io/en/latest/index.html>`_

Documentation guide
-------------------

* `Documentation Guide : Upload cython and python autodoc on ReadTheDoc <http://docguide.readthedocs.io/en/latest/index.html>`_

Git guide
---------

* `Git Guide <http://gitguide.readthedocs.io/en/latest/index.html>`_

Unix guide
----------

* `Unix Guide <http://unixguide.readthedocs.io/en/latest/index.html>`_


Text-editors (VIM and Sublime)
==============================

In these tutorials, keyboard-shortcuts for the text-editors are shown, 

* `Vim <http://vimguide.readthedocs.io/en/latest/index.html>`_

* `Sublime <http://sublimeguide.readthedocs.io/en/latest/index.html>`_
  

WEB Design
==========

This part contains various tutorials on web-designs, 

HTML, CSS, Bootstrap, JavaScript and jQuery
-------------------------------------------

Below link contains the tutorials on front-end web designs, 

* `Front-end tutorials <http://htmlguide.readthedocs.io>`_

Flask
-----

* `Flask <http://flaskguide.readthedocs.io>`_

Django
------

* `Bookstore using Django 1.11.10 (More on class-based-views) <http://djangoguide.readthedocs.io/en/latest/>`_
* `Bookstore using Django 1.8 (basics) <http://codehubdjango18.readthedocs.io/en/latest/>`_
* `Django with MySQL (old tutorial) <http://codehubpython.appspot.com/django>`_

Django Rest Api
---------------

* `Django Rest Api <http://codehubrestapi.readthedocs.io/en/latest/>`_

Django CMS
----------

* `Django CMS (Content Management System) <http://codehubdjangocms.readthedocs.io/en/latest/>`_

Selenium testing
----------------

* `MySql database and Selenium testing <http://codehubpython.appspot.com/>`_


TMUX for terminals
==================

TMUX can be used for providing the addition features to terminals such as split-screen etc.

* `TMUX Guide <http://tmuxguide.readthedocs.io/en/latest/>`_


Latex format for Thesis and Notes
=================================

* Latex format can be downloaded from below link (go to link and click on '**clone and download**' and '**Download Zip file**'),

    https://github.com/PythonDSP/Latex-Format-for-Thesis-and-Notes
    

Sphinx format for Latex and html
================================

Here, conf.py is modified for Latex and HTML documents. Also, some of the methods are discussed for creating document using Sphinx. 

* Online : http://sphinxguide.readthedocs.io/en/latest/
* Git: https://github.com/PythonDSP/Sphinx-Format-for-LatexPDF-HTML