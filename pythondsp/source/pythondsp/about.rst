About
=====

All tutorials are the personal-notes (for quick references) on various topics, which are based on experience, text-books and information on the Internet. The tutorials can be broadly divided into five categories i.e. ‘Theory’, ‘Simulation’, ‘Implementation’, 'Data processing' and 'Web Design'. 

Python language along with Numpy, Scipy, Matplotlib, Cython and Numba etc. are used for simulation purposes. VHDL, Verilog and Nios processors are discussed in the blog for implementation of the designs. Further, data processing using Python, Pandas and MySQL etc. are discussed in the tutorials. Lastly, some tutorials on the Web-design using HTML, JavaScript, and Django etc. are also provided.  

Publications
============

Following is the list of publications. 

2017
----

* Meher Krishna Patel, Stevan M. Berber, Kevin W. Sowerby, "Maximal Ratio Combining Using Channel Estimation in Chaos Based Pilot-Added DS-CDMA System with Antenna Diversity", Wireless Communications and Mobile Computing, Vol 2017, Article ID 3607167, https://doi.org/10.1155/2017/3607167. 

* Meher Krishna Patel, Stevan M. Berber, Kevin W. Sowerby, "Antijamming Performance of Adaptive Chaos Based CDMA System with MRC in Imperfect Channel Estimation Environment", Wireless Communications and Mobile Computing, Vol 2017, Article ID 2716949, https://doi.org/10.1155/2017/2716949. 

2016
----

* Meher Krishna Patel, Stevan M. Berber, Kevin W. Sowerby, “BER expression for maximum ratio combining in imperfect channel estimation environment”, Journal of communication engineering and systems, Vol. 5, No. 3, May-July 2016, pp. 27-33.

* Meher Krishna Patel, Stevan M. Berber, Kevin W. Sowerby, “Bayesian channel estimation in chaos based multicarrier CDMA system under slowly varying frequency selective channel,” International Journal of Circuits, Systems and Signal Processing Volume 10, (ISSN: 1998-4464) (2016) 62–68.


2015
----


* Meher Krishna Patel, Stevan M. Berber, Kevin W. Sowerby, “Adaptive chaos based CDMA system with antenna diversity in frequency selective channel,” Wireless Personal Communications, Springer, 2015, DOI:10.1007/s11277-015-2696-4.

* Meher Krishna Patel, Stevan M. Berber, Kevin W. Sowerby, “Adaptive RAKE receiver in chaos based pilot-added DS-CDMA system,” Physical Communication, Elsevier, 2015, DOI: 10.1016/j.phycom.2015.05.001.


* Meher Krishna Patel, Stevan M. Berber, Kevin W. Sowerby, “Bayesian Channel Estimation in Chaos Based DS-CDMA System,” in Proceedings of the International Conference on Electronics and Communication system, (ECS-2015), Barcelona, Spain, 7-9  April 2015, pp. 60-64. 
