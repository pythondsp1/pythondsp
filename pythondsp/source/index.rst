.. PythonDSP documentation master file, created by
   sphinx-quickstart on Tue Jan 31 19:26:23 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PythonDSP : Table of Contents
=============================

.. admonition:: Offline tutorials in PDF, EPUB and HTML formats
   :class: danger

   Click on the '**v.latest**' on the bottom-left in the tutorial website, and select the format for offline reading, as shown in below figure,  

   .. figure:: pythondsp/fig/download.png
   
      Download tutorials for offline reading 
   
   

.. toctree::
    :numbered:
    :includehidden:
    :caption: Contents:


    pythondsp/toc
    pythondsp/about
    pythondsp/disclaimer


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

